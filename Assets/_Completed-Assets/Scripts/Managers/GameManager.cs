using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Cinemachine;

namespace Complete
{
    public class GameManager : MonoBehaviour
    {
        public int maxJugadores = 4;
        public int m_NumRoundsToWin = 5;            // The number of rounds a single player has to win to win the game
        public float m_StartDelay = 3f;             // The delay between the start of RoundStarting and RoundPlaying phases
        public float m_EndDelay = 3f;               // The delay between the end of RoundPlaying and RoundEnding phases
        public CameraControl m_CameraControl;       // Reference to the CameraControl script for control during different phases
        public Text m_MessageText;                  // Reference to the overlay Text to display winning text, etc.
        public GameObject m_TankPrefab;             // Reference to the prefab the players will control
        public List<TankManager> m_Tanks;           // A collection of managers for enabling and disabling different aspects of the tanks
        public Transform [] positions;              //Posiciones de salida
        public List<GameObject> camaras;            //Camaras Brain de cada tanke
        public List<GameObject> camarasVirtuales;   //Camaras Virtuales de cada tanke

        private int m_RoundNumber;                  // Which round the game is currently on
        private WaitForSeconds m_StartWait;         // Used to have a delay whilst the round starts
        private WaitForSeconds m_EndWait;           // Used to have a delay whilst the round or game ends
        private TankManager m_RoundWinner;          // Reference to the winner of the current round.  Used to make an announcement of who won
        private TankManager m_GameWinner;           // Reference to the winner of the game.  Used to make an announcement of who won
        private Camera mainCam;                     //Camara principal

        private void Start()
        {
            // Create the delays so they only have to be made once
            m_StartWait = new WaitForSeconds (m_StartDelay);
            m_EndWait = new WaitForSeconds (m_EndDelay);

            SpawnAllTanks();
            SetCameraTargets();

            // Once the tanks have been created and the camera is using them as targets, start the game
            StartCoroutine (GameLoop());

            EventosJuego.agregarJugador = agregarJugador;
            EventosJuego.actualizarCamaras = ajustarCamaras;

        }

		
		private void SpawnAllTanks()
		{
			// For all the tanks...
			for (int i = 0; i < EventosJuego.jugadores; i++)
			{
                agregarJugador();
			}
		}

        private void SetCameraTargets()
        {
            // Create a collection of transforms the same size as the number of tanks
            Transform[] targets = new Transform[m_Tanks.Count];

            TankManager[] tankes = m_Tanks.ToArray();

            // For each of these transforms...
            for (int i = 0; i < targets.Length; i++)
            {
                // ... set it to the appropriate tank transform
                targets[i] = tankes[i].m_Instance.transform;
            }

            // These are the targets the camera should follow
            //m_CameraControl.m_Targets = targets;
        }


        // This is called from start and will run each phase of the game one after another
        private IEnumerator GameLoop()
        {
            // Start off by running the 'RoundStarting' coroutine but don't return until it's finished
            yield return StartCoroutine (RoundStarting());

            // Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished
            yield return StartCoroutine (RoundPlaying());

            // Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished
            yield return StartCoroutine (RoundEnding());

            // This code is not run until 'RoundEnding' has finished.  At which point, check if a game winner has been found
            if (m_GameWinner != null)
            {
                // If there is a game winner, restart the level
                SceneManager.LoadScene ("menu");
            }
            else
            {
                // If there isn't a winner yet, restart this coroutine so the loop continues
                // Note that this coroutine doesn't yield.  This means that the current version of the GameLoop will end
                StartCoroutine (GameLoop());
            }
        }


        private IEnumerator RoundStarting()
        {
            // As soon as the round starts reset the tanks and make sure they can't move
            ResetAllTanks();
            DisableTankControl();
            ajustarCamaras();

            // Increment the round number and display text showing the players what round it is
            m_RoundNumber++;
            m_MessageText.text = "ROUND " + m_RoundNumber;

            // Wait for the specified length of time until yielding control back to the game loop
            yield return m_StartWait;
        }


        private IEnumerator RoundPlaying()
        {
            // As soon as the round begins playing let the players control the tanks
            EnableTankControl();

            // Clear the text from the screen
            m_MessageText.text = string.Empty;

            // While there is not one tank left...
            while (!OneTankLeft())
            {
                // ... return on the next frame
                yield return null;
            }
        }


        private IEnumerator RoundEnding()
        {
            // Stop tanks from moving
            DisableTankControl();

            // Clear the winner from the previous round
            m_RoundWinner = null;

            // See if there is a winner now the round is over
            m_RoundWinner = GetRoundWinner();

            // If there is a winner, increment their score
            if (m_RoundWinner != null)
                m_RoundWinner.m_Wins++;

            // Now the winner's score has been incremented, see if someone has one the game
            m_GameWinner = GetGameWinner();

            // Get a message based on the scores and whether or not there is a game winner and display it
            string message = EndMessage();
            m_MessageText.text = message;

            // Wait for the specified length of time until yielding control back to the game loop
            yield return m_EndWait;
        }


        // This is used to check if there is one or fewer tanks remaining and thus the round should end
        private bool OneTankLeft()
        {
            // Start the count of tanks left at zero
            int numTanksLeft = 0;

            TankManager[] tankes = m_Tanks.ToArray();

            // Go through all the tanks...
            for (int i = 0; i < m_Tanks.Count; i++)
            {
                // ... and if they are active, increment the counter
                if (tankes[i].m_Instance.activeSelf)
                    numTanksLeft++;
            }

            // If there are one or fewer tanks remaining return true, otherwise return false
            return numTanksLeft <= 1;
        }
        
        
        // This function is to find out if there is a winner of the round
        // This function is called with the assumption that 1 or fewer tanks are currently active
        private TankManager GetRoundWinner()
        {
            TankManager[] tankes = m_Tanks.ToArray();

            // Go through all the tanks...
            for (int i = 0; i < m_Tanks.Count; i++)
            {
                // ... and if one of them is active, it is the winner so return it
                if (tankes[i].m_Instance.activeSelf)
                {
                    return tankes[i];
                }
            }

            // If none of the tanks are active it is a draw so return null
            return null;
        }


        // This function is to find out if there is a winner of the game
        private TankManager GetGameWinner()
        {
            TankManager[] tankes = m_Tanks.ToArray();

            // Go through all the tanks...
            for (int i = 0; i < m_Tanks.Count; i++)
            {
                // ... and if one of them has enough rounds to win the game, return it
                if (tankes[i].m_Wins == m_NumRoundsToWin)
                {
                    return tankes[i];
                }
            }

            // If no tanks have enough rounds to win, return null
            return null;
        }


        // Returns a string message to display at the end of each round.
        private string EndMessage()
        {
            // By default when a round ends there are no winners so the default end message is a draw
            string message = "DRAW!";

            TankManager[] tankes = m_Tanks.ToArray();

            // If there is a winner then change the message to reflect that
            if (m_RoundWinner != null)
            {
                message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";
            }

            // Add some line breaks after the initial message
            message += "\n\n\n\n";

            // Go through all the tanks and add each of their scores to the message
            for (int i = 0; i < m_Tanks.Count; i++)
            {
                message += tankes[i].m_ColoredPlayerText + ": " + tankes[i].m_Wins + " WINS\n";
            }

            // If there is a game winner, change the entire message to reflect that
            if (m_GameWinner != null)
            {
                message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";
            }

            return message;
        }


        // This function is used to turn all the tanks back on and reset their positions and properties
        private void ResetAllTanks()
        {
            TankManager[] tankes = m_Tanks.ToArray();
            for (int i = 0; i < m_Tanks.Count; i++)
            {
                tankes[i].Reset();
            }
        }


        private void EnableTankControl()
        {
            TankManager[] tankes = m_Tanks.ToArray();
            for (int i = 0; i < m_Tanks.Count; i++)
            {
                tankes[i].EnableControl();
            }
        }


        private void DisableTankControl()
        {
            TankManager[] tankes = m_Tanks.ToArray();
            for (int i = 0; i < m_Tanks.Count; i++)
            {
                tankes[i].DisableControl();
            }
        }

        private void agregarJugador()
        {
            if(m_Tanks.Count < maxJugadores)
            {
                TankManager tank = new TankManager();  
                tank.m_Instance = Instantiate(m_TankPrefab, positions[m_Tanks.Count].position, positions[m_Tanks.Count].rotation) as GameObject;
                tank.m_PlayerNumber = m_Tanks.Count + 1;
                tank.m_SpawnPoint = positions[m_Tanks.Count];
                tank.m_PlayerColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
                tank.camera_instance = camaras[m_Tanks.Count];
                camarasVirtuales[m_Tanks.Count].GetComponent<CinemachineVirtualCamera>().LookAt = tank.m_Instance.transform;
                camarasVirtuales[m_Tanks.Count].GetComponent<CinemachineVirtualCamera>().Follow = tank.m_Instance.transform;
                tank.Setup();
                m_Tanks.Add(tank);
                ajustarCamaras();
            }
   
        }

        private void ajustarCamaras()
        {
            int jugadores = 0;
            TankManager p1 = null, p2 = null, p3 = null, p4 = null;
            foreach(TankManager tank in m_Tanks)
            {
                if (!tank.m_Health.m_Dead)
                {
                    if(jugadores == 0)
                    {
                        p1 = tank;
                    }else if(jugadores == 1)
                    {
                        p2 = tank;
                    }else if(jugadores == 2)
                    {
                        p3 = tank;
                    }else if(jugadores == 3)
                    {
                        p4 = tank;
                    }
                    jugadores++;
                }
                else
                {
                    tank.camera_instance.SetActive(false);
                }
            }

            if (jugadores == 2)
            {
                p1.camera_instance.GetComponent<Camera>().rect = new Rect(0f, 0f, 1f, 0.5f);
                p1.camera_instance.SetActive(true);
                p2.camera_instance.GetComponent<Camera>().rect = new Rect(0f, 0.5f, 1f, 0.5f);
                p2.camera_instance.SetActive(true);

            }
            else if (jugadores == 3)
            {
                p1.camera_instance.GetComponent<Camera>().rect = new Rect(0f, 0f, 0.5f, 0.5f);
                p1.camera_instance.SetActive(true);
                p2.camera_instance.GetComponent<Camera>().rect = new Rect(0f, 0.5f, 0.5f, 0.5f);
                p2.camera_instance.SetActive(true);
                p3.camera_instance.GetComponent<Camera>().rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                p3.camera_instance.SetActive(true);
            }
            else if(jugadores == 4)
            {
                p1.camera_instance.GetComponent<Camera>().rect = new Rect(0f, 0f, 0.5f, 0.5f);
                p1.camera_instance.SetActive(true);
                p2.camera_instance.GetComponent<Camera>().rect = new Rect(0f, 0.5f, 0.5f, 0.5f);
                p2.camera_instance.SetActive(true);
                p3.camera_instance.GetComponent<Camera>().rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                p3.camera_instance.SetActive(true);
                p4.camera_instance.GetComponent<Camera>().rect = new Rect(0.5f, 0f, 0.5f, 0.5f);
                p4.camera_instance.SetActive(true);
            }
            else
            {
                p1.camera_instance.GetComponent<Camera>().rect = new Rect(0f, 0f, 1f, 1f);
                p1.camera_instance.SetActive(true);
            }

            if (jugadores == 3)
            {
                //mostramos minimapa
                camaras[4].SetActive(true);
                camaras[4].GetComponent<Camera>().rect = new Rect(0.5f,0f,0.5f,0.5f);
            }
            else
            {
                //no mostramos minimapa
                camaras[4].SetActive(false);
            }
        }
    }

}