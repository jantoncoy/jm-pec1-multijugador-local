﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Complete
{
    public class TankMovement : MonoBehaviour
    {
        public int m_PlayerNumber = 1;              // Used to identify which tank belongs to which player. This is set by this tank's manager
        public float m_Speed = 12f;                 // How fast the tank moves forward and back
        public float m_TurnSpeed = 180f;            // How fast the tank turns in degrees per second
        public AudioSource m_MovementAudio;         // Reference to the audio source used to play engine sounds. NB: different to the shooting audio source
        public AudioClip m_EngineIdling;            // Audio to play when the tank isn't moving
        public AudioClip m_EngineDriving;           // Audio to play when the tank is moving
		public float m_PitchRange = 0.2f;           // The amount by which the pitch of the engine noises can vary
        public InputController ic;                  // Clase del input controller
        public PlayerInput playerInput;             // Clase del input contoller
        public string schemaIC = "p1";                     // Esquema para el input controller

        private string m_MovementAxisName;          // The name of the input axis for moving forward and back
        private string m_TurnAxisName;              // The name of the input axis for turning
        private Rigidbody m_Rigidbody;              // Reference used to move the tank
        private float m_MovementInputValue;         // The current value of the movement input
        private float m_TurnInputValue;             // The current value of the turn input
        private float m_OriginalPitch;              // The pitch of the audio source at the start of the scene

        private bool arriba, abajo, izquierda, derecha;

        private void Awake()
        {

            m_Rigidbody = GetComponent<Rigidbody>();
            //Instanciamos methods de input controller
            ic = new InputController();


        }

        public void actualizarControles()
        {
            if (m_PlayerNumber == 1)
            {
                //Se pulsa
                ic.player1.arriba.performed += ctx => { arriba = true; };
                ic.player1.abajo.performed += ctx => { abajo = true; };
                ic.player1.izquierda.performed += ctx => { izquierda = true; };
                ic.player1.derecha.performed += ctx => { derecha = true; };

                //Se deja de pulsar
                ic.player1.arriba.canceled += ctx => { arriba = false; };
                ic.player1.abajo.canceled += ctx => { abajo = false; };
                ic.player1.izquierda.canceled += ctx => { izquierda = false; };
                ic.player1.derecha.canceled += ctx => { derecha = false; };
            }
            else if (m_PlayerNumber == 2)
            {
                //Se pulsa
                ic.player2.arriba.performed += ctx => { arriba = true; };
                ic.player2.abajo.performed += ctx => { abajo = true; };
                ic.player2.izquierda.performed += ctx => { izquierda = true; };
                ic.player2.derecha.performed += ctx => { derecha = true; };

                //Se deja de pulsar
                ic.player2.arriba.canceled += ctx => { arriba = false; };
                ic.player2.abajo.canceled += ctx => { abajo = false; };
                ic.player2.izquierda.canceled += ctx => { izquierda = false; };
                ic.player2.derecha.canceled += ctx => { derecha = false; };
            }
            else if (m_PlayerNumber == 3)
            {
                //Se pulsa
                ic.player3.arriba.performed += ctx => { arriba = true; };
                ic.player3.abajo.performed += ctx => { abajo = true; };
                ic.player3.izquierda.performed += ctx => { izquierda = true; };
                ic.player3.derecha.performed += ctx => { derecha = true; };

                //Se deja de pulsar
                ic.player3.arriba.canceled += ctx => { arriba = false; };
                ic.player3.abajo.canceled += ctx => { abajo = false; };
                ic.player3.izquierda.canceled += ctx => { izquierda = false; };
                ic.player3.derecha.canceled += ctx => { derecha = false; };
            }
            else if (m_PlayerNumber == 4)
            {
                //Se pulsa
                ic.player4.arriba.performed += ctx => { arriba = true; };
                ic.player4.abajo.performed += ctx => { abajo = true; };
                ic.player4.izquierda.performed += ctx => { izquierda = true; };
                ic.player4.derecha.performed += ctx => { derecha = true; };

                //Se deja de pulsar
                ic.player4.arriba.canceled += ctx => { arriba = false; };
                ic.player4.abajo.canceled += ctx => { abajo = false; };
                ic.player4.izquierda.canceled += ctx => { izquierda = false; };
                ic.player4.derecha.canceled += ctx => { derecha = false; };
            }
        }

        private void OnEnable()
        {
            // When the tank is turned on, make sure it's not kinematic
            m_Rigidbody.isKinematic = false;

            // Also reset the input values
            m_MovementInputValue = 0f;
            m_TurnInputValue = 0f;

            //Habilitamos controles
            ic.Enable();
        }


        private void OnDisable()
        {
            // When the tank is turned off, set it to kinematic so it stops moving
            m_Rigidbody.isKinematic = true;

            //Deshabilitamos controles
            ic.Disable();
        }


        private void Start()
        {
            // The axes names are based on player number
            m_MovementAxisName = "Vertical" + m_PlayerNumber;
            m_TurnAxisName = "Horizontal" + m_PlayerNumber;

            // Store the original pitch of the audio source
            m_OriginalPitch = m_MovementAudio.pitch;
        }


        private void Update ()
        {
            // Store the value of both input axes
            m_MovementInputValue = calcularMove();
            m_TurnInputValue = calcularTurn();

            EngineAudio();
        }

        /// <summary>
        /// Calculamos el movimiento segun las pulsaciones
        /// </summary>
        /// <returns></returns>
        private float calcularMove()
        {
            float valor = 0;

            if (arriba)
            {
                valor += 1.0f;
            }

            if (abajo)
            {
                valor += -0.5f;
            }

            return valor;
        }

        /// <summary>
        /// Calculamos el giro segun las pulsaciones
        /// </summary>
        /// <returns></returns>
        private float calcularTurn()
        {
            float valor = 0;

            if (derecha)
            {
                valor += 1.0f;
            }

            if (izquierda)
            {
                valor += -1.0f;
            }

            return valor;
        }

        private void EngineAudio()
        {
            // If there is no input (the tank is stationary)...
            if (Mathf.Abs (m_MovementInputValue) < 0.1f && Mathf.Abs (m_TurnInputValue) < 0.1f)
            {
                // ... and if the audio source is currently playing the driving clip...
                if (m_MovementAudio.clip == m_EngineDriving)
                {
                    // ... change the clip to idling and play it
                    m_MovementAudio.clip = m_EngineIdling;
                    m_MovementAudio.pitch = Random.Range (m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
                    m_MovementAudio.Play ();
                }
            }
            else
            {
                // Otherwise if the tank is moving and if the idling clip is currently playing...
                if (m_MovementAudio.clip == m_EngineIdling)
                {
                    // ... change the clip to driving and play
                    m_MovementAudio.clip = m_EngineDriving;
                    m_MovementAudio.pitch = Random.Range(m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
                    m_MovementAudio.Play();
                }
            }
        }


        private void FixedUpdate()
        {
            // Adjust the rigidbodies position and orientation in FixedUpdate.
            Move();
            Turn();
        }


        private void Move()
        {
            // Create a vector in the direction the tank is facing with a magnitude based on the input, speed and the time between frames
            Vector3 movement = transform.forward * m_MovementInputValue * m_Speed * Time.deltaTime;

            // Apply this movement to the rigidbody's position
            m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
        }


        private void Turn()
        {
            // Determine the number of degrees to be turned based on the input, speed and time between frames
            float turn = m_TurnInputValue * m_TurnSpeed * Time.deltaTime;

            // Make this into a rotation in the y axis
            Quaternion turnRotation = Quaternion.Euler (0f, turn, 0f);

            // Apply this rotation to the rigidbody's rotation
            m_Rigidbody.MoveRotation (m_Rigidbody.rotation * turnRotation);
        }
    }
}