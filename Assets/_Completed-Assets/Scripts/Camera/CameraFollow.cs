﻿using UnityEngine;

namespace Complete {
	
    public class CameraFollow : MonoBehaviour {
        
        private Camera m_Camera;                        // Camera reference              
        public float smooth = 0.5f;                     // Smoothness
		public float limitDist = 20.0f;                 // Distance limit
       
        private void FixedUpdate ()
        {
			if (m_Camera == null) {
				m_Camera = GetComponentInChildren<Camera>();
				return;
			}

            Follow();
        }


		private void Follow()
        {
			float currentDist = Vector2.Distance (new Vector2(transform.position.x,transform.position.z), new Vector2(m_Camera.transform.position.x,m_Camera.transform.position.z));

            m_Camera.transform.position = new Vector3(transform.position.x,m_Camera.transform.position.y,transform.position.z);

        }
	}
}