using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void iniciar(int jugadores)
    {
        EventosJuego.jugadores = jugadores;
        SceneManager.LoadScene("_Complete-Game");
    }

    public void salir()
    {
        Application.Quit(1);
    }
}
