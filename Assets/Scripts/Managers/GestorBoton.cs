using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestorBoton : MonoBehaviour
{
    /// <summary>
    /// Instancia de boton para agregar jugadores
    /// </summary>
    public GameObject botonAgregar;

    // Update is called once per frame
    void Update()
    {
        if(EventosJuego.jugadores == EventosJuego.maxJugadores)
        {
            botonAgregar.SetActive(false);
        }
    }
}
