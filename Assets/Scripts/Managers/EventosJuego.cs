using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventosJuego
{
    /// <summary>
    /// Jugadores maximos
    /// </summary>
    public const int maxJugadores = 4;

    /// <summary>
    /// Indica el numero de jugadores al empezar la partida
    /// </summary>
    public static int jugadores = 2;

    /// <summary>
    /// Callback que agrega un jugador al juego
    /// </summary>
    public static Action agregarJugador;

    /// <summary>
    /// Callback actualizarCamaras
    /// </summary>
    public static Action actualizarCamaras;

}
