using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddPlayer : MonoBehaviour
{
    // Update is called once per frame
    public void addPlayer()
    {
        if(EventosJuego.jugadores < 4)
        {
            EventosJuego.agregarJugador();
            EventosJuego.jugadores += 1;
        }
    }
}
