// GENERATED AUTOMATICALLY FROM 'Assets/InputController.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputController : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputController()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputController"",
    ""maps"": [
        {
            ""name"": ""player1"",
            ""id"": ""0adc68b4-9c09-45bd-a9c6-8f314a83193c"",
            ""actions"": [
                {
                    ""name"": ""arriba"",
                    ""type"": ""Button"",
                    ""id"": ""bf4e2350-4e57-4548-b280-aed0d85c8033"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""abajo"",
                    ""type"": ""Button"",
                    ""id"": ""0e4fa7f2-2467-4164-bc02-7faa7b2f9421"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""izquierda"",
                    ""type"": ""Button"",
                    ""id"": ""abe73561-71db-4d4b-9b72-41020279e293"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""derecha"",
                    ""type"": ""Button"",
                    ""id"": ""50d6b585-9f6e-40f9-8f66-feb51e3f45fe"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""disparo"",
                    ""type"": ""Button"",
                    ""id"": ""c3b38580-6d7d-4e49-9e01-a74758841933"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""11fc80ee-5fc7-4b12-9b34-68e6d5356101"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""arriba"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""38f7f140-73b7-4365-b9be-6245744c7982"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""abajo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""77d562be-6cbd-497e-9711-d8c64ae4a868"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""izquierda"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3e75f9d2-8422-4a1b-a801-f441ce9467d1"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""derecha"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ba0a42e4-f286-4878-a07a-e56de653f940"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""disparo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""player2"",
            ""id"": ""caed9e6a-635b-421d-a817-a48c7feb16ea"",
            ""actions"": [
                {
                    ""name"": ""arriba"",
                    ""type"": ""Button"",
                    ""id"": ""a77e7a77-02fd-4526-bc2f-d2250d3bae5b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""abajo"",
                    ""type"": ""Button"",
                    ""id"": ""a6da6017-96e7-46e4-90ac-ea262690bd32"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""izquierda"",
                    ""type"": ""Button"",
                    ""id"": ""fc7c16ec-51b1-412e-934e-0a87d19a014e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""derecha"",
                    ""type"": ""Button"",
                    ""id"": ""82329949-1a3b-4440-992c-087dfa8b920b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""disparo"",
                    ""type"": ""Button"",
                    ""id"": ""8e4c3581-197e-47a3-afd3-c00f40f2fb43"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""6a58ba6b-3c9b-4818-b456-4a1b4f920498"",
                    ""path"": ""<Keyboard>/t"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""arriba"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a5544bdd-a316-43ba-90bc-145791ed22ea"",
                    ""path"": ""<Keyboard>/g"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""abajo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""16a05343-91f3-4b8b-8edf-beb542d5f2e2"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""izquierda"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1420f757-211d-45bb-b9b9-dfc15b82cd3d"",
                    ""path"": ""<Keyboard>/h"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""derecha"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7d9b6e6a-a4d9-4d7a-97da-4781fb7531c7"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""disparo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""player3"",
            ""id"": ""43b85327-df7d-4cbd-a41c-c91a7fb9406e"",
            ""actions"": [
                {
                    ""name"": ""arriba"",
                    ""type"": ""Button"",
                    ""id"": ""0793d28d-fc47-4631-8331-f207524ecc6b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""abajo"",
                    ""type"": ""Button"",
                    ""id"": ""5ae1a65c-c854-4b1a-b176-ba6d0bb801d9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""izquierda"",
                    ""type"": ""Button"",
                    ""id"": ""75481945-c6b4-43af-83d6-0c5341e7d6c2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""derecha"",
                    ""type"": ""Button"",
                    ""id"": ""efd6cb48-0999-4ee0-b2bc-34e7a64aa290"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""disparo"",
                    ""type"": ""Button"",
                    ""id"": ""f4e36a58-39b8-4814-bd4a-46fa7f42557a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""2fbca392-1986-415e-90ba-d1120e4bf900"",
                    ""path"": ""<Keyboard>/i"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""arriba"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c2ba0f7e-19f9-40d0-897a-4f4752dfd1cc"",
                    ""path"": ""<Keyboard>/k"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""abajo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e5cb3d2d-04ef-4006-81cf-65c5cf53f70c"",
                    ""path"": ""<Keyboard>/j"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""izquierda"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""75ec026a-fd3b-4955-8705-b7e125d41458"",
                    ""path"": ""<Keyboard>/l"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""derecha"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""92c5e246-c88e-4f24-9882-e80b09bd76e6"",
                    ""path"": ""<Keyboard>/u"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""disparo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""player4"",
            ""id"": ""a283089f-eebf-4840-bd1c-214dcb5af896"",
            ""actions"": [
                {
                    ""name"": ""arriba"",
                    ""type"": ""Button"",
                    ""id"": ""c64ce5ce-1ddc-4588-9712-e3d8d4385bca"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""abajo"",
                    ""type"": ""Button"",
                    ""id"": ""c84ad3df-43f9-4c74-b660-0d4a5ed361ac"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""izquierda"",
                    ""type"": ""Button"",
                    ""id"": ""d6e94716-fad2-4214-8023-140393b5e32c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""derecha"",
                    ""type"": ""Button"",
                    ""id"": ""928db53d-8cc8-4e80-b684-0e5f3103273b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""disparo"",
                    ""type"": ""Button"",
                    ""id"": ""bff1a796-8df1-4072-8a92-db7105bdcbad"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""6756061d-d0a9-4998-bea9-b73f671c8874"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""arriba"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""56a7c532-d7ea-4544-be4e-9e6e09ca6f72"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""abajo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""aeb349db-21d9-4a10-bd3e-c393aa63ac3f"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""izquierda"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""292ba419-5c8f-448a-a414-6eb3746bbe41"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""derecha"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c1962fbb-df46-489e-93a9-7e1359cfdc20"",
                    ""path"": ""<Keyboard>/slash"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""disparo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c97a3c82-4e70-4443-aafb-3dbdc3ec957d"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""disparo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b8fda92d-49d7-4da0-aa10-ef334bc1e9a0"",
                    ""path"": ""<Keyboard>/rightShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""disparo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // player1
        m_player1 = asset.FindActionMap("player1", throwIfNotFound: true);
        m_player1_arriba = m_player1.FindAction("arriba", throwIfNotFound: true);
        m_player1_abajo = m_player1.FindAction("abajo", throwIfNotFound: true);
        m_player1_izquierda = m_player1.FindAction("izquierda", throwIfNotFound: true);
        m_player1_derecha = m_player1.FindAction("derecha", throwIfNotFound: true);
        m_player1_disparo = m_player1.FindAction("disparo", throwIfNotFound: true);
        // player2
        m_player2 = asset.FindActionMap("player2", throwIfNotFound: true);
        m_player2_arriba = m_player2.FindAction("arriba", throwIfNotFound: true);
        m_player2_abajo = m_player2.FindAction("abajo", throwIfNotFound: true);
        m_player2_izquierda = m_player2.FindAction("izquierda", throwIfNotFound: true);
        m_player2_derecha = m_player2.FindAction("derecha", throwIfNotFound: true);
        m_player2_disparo = m_player2.FindAction("disparo", throwIfNotFound: true);
        // player3
        m_player3 = asset.FindActionMap("player3", throwIfNotFound: true);
        m_player3_arriba = m_player3.FindAction("arriba", throwIfNotFound: true);
        m_player3_abajo = m_player3.FindAction("abajo", throwIfNotFound: true);
        m_player3_izquierda = m_player3.FindAction("izquierda", throwIfNotFound: true);
        m_player3_derecha = m_player3.FindAction("derecha", throwIfNotFound: true);
        m_player3_disparo = m_player3.FindAction("disparo", throwIfNotFound: true);
        // player4
        m_player4 = asset.FindActionMap("player4", throwIfNotFound: true);
        m_player4_arriba = m_player4.FindAction("arriba", throwIfNotFound: true);
        m_player4_abajo = m_player4.FindAction("abajo", throwIfNotFound: true);
        m_player4_izquierda = m_player4.FindAction("izquierda", throwIfNotFound: true);
        m_player4_derecha = m_player4.FindAction("derecha", throwIfNotFound: true);
        m_player4_disparo = m_player4.FindAction("disparo", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // player1
    private readonly InputActionMap m_player1;
    private IPlayer1Actions m_Player1ActionsCallbackInterface;
    private readonly InputAction m_player1_arriba;
    private readonly InputAction m_player1_abajo;
    private readonly InputAction m_player1_izquierda;
    private readonly InputAction m_player1_derecha;
    private readonly InputAction m_player1_disparo;
    public struct Player1Actions
    {
        private @InputController m_Wrapper;
        public Player1Actions(@InputController wrapper) { m_Wrapper = wrapper; }
        public InputAction @arriba => m_Wrapper.m_player1_arriba;
        public InputAction @abajo => m_Wrapper.m_player1_abajo;
        public InputAction @izquierda => m_Wrapper.m_player1_izquierda;
        public InputAction @derecha => m_Wrapper.m_player1_derecha;
        public InputAction @disparo => m_Wrapper.m_player1_disparo;
        public InputActionMap Get() { return m_Wrapper.m_player1; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Player1Actions set) { return set.Get(); }
        public void SetCallbacks(IPlayer1Actions instance)
        {
            if (m_Wrapper.m_Player1ActionsCallbackInterface != null)
            {
                @arriba.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnArriba;
                @arriba.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnArriba;
                @arriba.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnArriba;
                @abajo.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnAbajo;
                @abajo.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnAbajo;
                @abajo.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnAbajo;
                @izquierda.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnIzquierda;
                @izquierda.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnIzquierda;
                @izquierda.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnIzquierda;
                @derecha.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnDerecha;
                @derecha.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnDerecha;
                @derecha.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnDerecha;
                @disparo.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnDisparo;
                @disparo.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnDisparo;
                @disparo.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnDisparo;
            }
            m_Wrapper.m_Player1ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @arriba.started += instance.OnArriba;
                @arriba.performed += instance.OnArriba;
                @arriba.canceled += instance.OnArriba;
                @abajo.started += instance.OnAbajo;
                @abajo.performed += instance.OnAbajo;
                @abajo.canceled += instance.OnAbajo;
                @izquierda.started += instance.OnIzquierda;
                @izquierda.performed += instance.OnIzquierda;
                @izquierda.canceled += instance.OnIzquierda;
                @derecha.started += instance.OnDerecha;
                @derecha.performed += instance.OnDerecha;
                @derecha.canceled += instance.OnDerecha;
                @disparo.started += instance.OnDisparo;
                @disparo.performed += instance.OnDisparo;
                @disparo.canceled += instance.OnDisparo;
            }
        }
    }
    public Player1Actions @player1 => new Player1Actions(this);

    // player2
    private readonly InputActionMap m_player2;
    private IPlayer2Actions m_Player2ActionsCallbackInterface;
    private readonly InputAction m_player2_arriba;
    private readonly InputAction m_player2_abajo;
    private readonly InputAction m_player2_izquierda;
    private readonly InputAction m_player2_derecha;
    private readonly InputAction m_player2_disparo;
    public struct Player2Actions
    {
        private @InputController m_Wrapper;
        public Player2Actions(@InputController wrapper) { m_Wrapper = wrapper; }
        public InputAction @arriba => m_Wrapper.m_player2_arriba;
        public InputAction @abajo => m_Wrapper.m_player2_abajo;
        public InputAction @izquierda => m_Wrapper.m_player2_izquierda;
        public InputAction @derecha => m_Wrapper.m_player2_derecha;
        public InputAction @disparo => m_Wrapper.m_player2_disparo;
        public InputActionMap Get() { return m_Wrapper.m_player2; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Player2Actions set) { return set.Get(); }
        public void SetCallbacks(IPlayer2Actions instance)
        {
            if (m_Wrapper.m_Player2ActionsCallbackInterface != null)
            {
                @arriba.started -= m_Wrapper.m_Player2ActionsCallbackInterface.OnArriba;
                @arriba.performed -= m_Wrapper.m_Player2ActionsCallbackInterface.OnArriba;
                @arriba.canceled -= m_Wrapper.m_Player2ActionsCallbackInterface.OnArriba;
                @abajo.started -= m_Wrapper.m_Player2ActionsCallbackInterface.OnAbajo;
                @abajo.performed -= m_Wrapper.m_Player2ActionsCallbackInterface.OnAbajo;
                @abajo.canceled -= m_Wrapper.m_Player2ActionsCallbackInterface.OnAbajo;
                @izquierda.started -= m_Wrapper.m_Player2ActionsCallbackInterface.OnIzquierda;
                @izquierda.performed -= m_Wrapper.m_Player2ActionsCallbackInterface.OnIzquierda;
                @izquierda.canceled -= m_Wrapper.m_Player2ActionsCallbackInterface.OnIzquierda;
                @derecha.started -= m_Wrapper.m_Player2ActionsCallbackInterface.OnDerecha;
                @derecha.performed -= m_Wrapper.m_Player2ActionsCallbackInterface.OnDerecha;
                @derecha.canceled -= m_Wrapper.m_Player2ActionsCallbackInterface.OnDerecha;
                @disparo.started -= m_Wrapper.m_Player2ActionsCallbackInterface.OnDisparo;
                @disparo.performed -= m_Wrapper.m_Player2ActionsCallbackInterface.OnDisparo;
                @disparo.canceled -= m_Wrapper.m_Player2ActionsCallbackInterface.OnDisparo;
            }
            m_Wrapper.m_Player2ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @arriba.started += instance.OnArriba;
                @arriba.performed += instance.OnArriba;
                @arriba.canceled += instance.OnArriba;
                @abajo.started += instance.OnAbajo;
                @abajo.performed += instance.OnAbajo;
                @abajo.canceled += instance.OnAbajo;
                @izquierda.started += instance.OnIzquierda;
                @izquierda.performed += instance.OnIzquierda;
                @izquierda.canceled += instance.OnIzquierda;
                @derecha.started += instance.OnDerecha;
                @derecha.performed += instance.OnDerecha;
                @derecha.canceled += instance.OnDerecha;
                @disparo.started += instance.OnDisparo;
                @disparo.performed += instance.OnDisparo;
                @disparo.canceled += instance.OnDisparo;
            }
        }
    }
    public Player2Actions @player2 => new Player2Actions(this);

    // player3
    private readonly InputActionMap m_player3;
    private IPlayer3Actions m_Player3ActionsCallbackInterface;
    private readonly InputAction m_player3_arriba;
    private readonly InputAction m_player3_abajo;
    private readonly InputAction m_player3_izquierda;
    private readonly InputAction m_player3_derecha;
    private readonly InputAction m_player3_disparo;
    public struct Player3Actions
    {
        private @InputController m_Wrapper;
        public Player3Actions(@InputController wrapper) { m_Wrapper = wrapper; }
        public InputAction @arriba => m_Wrapper.m_player3_arriba;
        public InputAction @abajo => m_Wrapper.m_player3_abajo;
        public InputAction @izquierda => m_Wrapper.m_player3_izquierda;
        public InputAction @derecha => m_Wrapper.m_player3_derecha;
        public InputAction @disparo => m_Wrapper.m_player3_disparo;
        public InputActionMap Get() { return m_Wrapper.m_player3; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Player3Actions set) { return set.Get(); }
        public void SetCallbacks(IPlayer3Actions instance)
        {
            if (m_Wrapper.m_Player3ActionsCallbackInterface != null)
            {
                @arriba.started -= m_Wrapper.m_Player3ActionsCallbackInterface.OnArriba;
                @arriba.performed -= m_Wrapper.m_Player3ActionsCallbackInterface.OnArriba;
                @arriba.canceled -= m_Wrapper.m_Player3ActionsCallbackInterface.OnArriba;
                @abajo.started -= m_Wrapper.m_Player3ActionsCallbackInterface.OnAbajo;
                @abajo.performed -= m_Wrapper.m_Player3ActionsCallbackInterface.OnAbajo;
                @abajo.canceled -= m_Wrapper.m_Player3ActionsCallbackInterface.OnAbajo;
                @izquierda.started -= m_Wrapper.m_Player3ActionsCallbackInterface.OnIzquierda;
                @izquierda.performed -= m_Wrapper.m_Player3ActionsCallbackInterface.OnIzquierda;
                @izquierda.canceled -= m_Wrapper.m_Player3ActionsCallbackInterface.OnIzquierda;
                @derecha.started -= m_Wrapper.m_Player3ActionsCallbackInterface.OnDerecha;
                @derecha.performed -= m_Wrapper.m_Player3ActionsCallbackInterface.OnDerecha;
                @derecha.canceled -= m_Wrapper.m_Player3ActionsCallbackInterface.OnDerecha;
                @disparo.started -= m_Wrapper.m_Player3ActionsCallbackInterface.OnDisparo;
                @disparo.performed -= m_Wrapper.m_Player3ActionsCallbackInterface.OnDisparo;
                @disparo.canceled -= m_Wrapper.m_Player3ActionsCallbackInterface.OnDisparo;
            }
            m_Wrapper.m_Player3ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @arriba.started += instance.OnArriba;
                @arriba.performed += instance.OnArriba;
                @arriba.canceled += instance.OnArriba;
                @abajo.started += instance.OnAbajo;
                @abajo.performed += instance.OnAbajo;
                @abajo.canceled += instance.OnAbajo;
                @izquierda.started += instance.OnIzquierda;
                @izquierda.performed += instance.OnIzquierda;
                @izquierda.canceled += instance.OnIzquierda;
                @derecha.started += instance.OnDerecha;
                @derecha.performed += instance.OnDerecha;
                @derecha.canceled += instance.OnDerecha;
                @disparo.started += instance.OnDisparo;
                @disparo.performed += instance.OnDisparo;
                @disparo.canceled += instance.OnDisparo;
            }
        }
    }
    public Player3Actions @player3 => new Player3Actions(this);

    // player4
    private readonly InputActionMap m_player4;
    private IPlayer4Actions m_Player4ActionsCallbackInterface;
    private readonly InputAction m_player4_arriba;
    private readonly InputAction m_player4_abajo;
    private readonly InputAction m_player4_izquierda;
    private readonly InputAction m_player4_derecha;
    private readonly InputAction m_player4_disparo;
    public struct Player4Actions
    {
        private @InputController m_Wrapper;
        public Player4Actions(@InputController wrapper) { m_Wrapper = wrapper; }
        public InputAction @arriba => m_Wrapper.m_player4_arriba;
        public InputAction @abajo => m_Wrapper.m_player4_abajo;
        public InputAction @izquierda => m_Wrapper.m_player4_izquierda;
        public InputAction @derecha => m_Wrapper.m_player4_derecha;
        public InputAction @disparo => m_Wrapper.m_player4_disparo;
        public InputActionMap Get() { return m_Wrapper.m_player4; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Player4Actions set) { return set.Get(); }
        public void SetCallbacks(IPlayer4Actions instance)
        {
            if (m_Wrapper.m_Player4ActionsCallbackInterface != null)
            {
                @arriba.started -= m_Wrapper.m_Player4ActionsCallbackInterface.OnArriba;
                @arriba.performed -= m_Wrapper.m_Player4ActionsCallbackInterface.OnArriba;
                @arriba.canceled -= m_Wrapper.m_Player4ActionsCallbackInterface.OnArriba;
                @abajo.started -= m_Wrapper.m_Player4ActionsCallbackInterface.OnAbajo;
                @abajo.performed -= m_Wrapper.m_Player4ActionsCallbackInterface.OnAbajo;
                @abajo.canceled -= m_Wrapper.m_Player4ActionsCallbackInterface.OnAbajo;
                @izquierda.started -= m_Wrapper.m_Player4ActionsCallbackInterface.OnIzquierda;
                @izquierda.performed -= m_Wrapper.m_Player4ActionsCallbackInterface.OnIzquierda;
                @izquierda.canceled -= m_Wrapper.m_Player4ActionsCallbackInterface.OnIzquierda;
                @derecha.started -= m_Wrapper.m_Player4ActionsCallbackInterface.OnDerecha;
                @derecha.performed -= m_Wrapper.m_Player4ActionsCallbackInterface.OnDerecha;
                @derecha.canceled -= m_Wrapper.m_Player4ActionsCallbackInterface.OnDerecha;
                @disparo.started -= m_Wrapper.m_Player4ActionsCallbackInterface.OnDisparo;
                @disparo.performed -= m_Wrapper.m_Player4ActionsCallbackInterface.OnDisparo;
                @disparo.canceled -= m_Wrapper.m_Player4ActionsCallbackInterface.OnDisparo;
            }
            m_Wrapper.m_Player4ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @arriba.started += instance.OnArriba;
                @arriba.performed += instance.OnArriba;
                @arriba.canceled += instance.OnArriba;
                @abajo.started += instance.OnAbajo;
                @abajo.performed += instance.OnAbajo;
                @abajo.canceled += instance.OnAbajo;
                @izquierda.started += instance.OnIzquierda;
                @izquierda.performed += instance.OnIzquierda;
                @izquierda.canceled += instance.OnIzquierda;
                @derecha.started += instance.OnDerecha;
                @derecha.performed += instance.OnDerecha;
                @derecha.canceled += instance.OnDerecha;
                @disparo.started += instance.OnDisparo;
                @disparo.performed += instance.OnDisparo;
                @disparo.canceled += instance.OnDisparo;
            }
        }
    }
    public Player4Actions @player4 => new Player4Actions(this);
    public interface IPlayer1Actions
    {
        void OnArriba(InputAction.CallbackContext context);
        void OnAbajo(InputAction.CallbackContext context);
        void OnIzquierda(InputAction.CallbackContext context);
        void OnDerecha(InputAction.CallbackContext context);
        void OnDisparo(InputAction.CallbackContext context);
    }
    public interface IPlayer2Actions
    {
        void OnArriba(InputAction.CallbackContext context);
        void OnAbajo(InputAction.CallbackContext context);
        void OnIzquierda(InputAction.CallbackContext context);
        void OnDerecha(InputAction.CallbackContext context);
        void OnDisparo(InputAction.CallbackContext context);
    }
    public interface IPlayer3Actions
    {
        void OnArriba(InputAction.CallbackContext context);
        void OnAbajo(InputAction.CallbackContext context);
        void OnIzquierda(InputAction.CallbackContext context);
        void OnDerecha(InputAction.CallbackContext context);
        void OnDisparo(InputAction.CallbackContext context);
    }
    public interface IPlayer4Actions
    {
        void OnArriba(InputAction.CallbackContext context);
        void OnAbajo(InputAction.CallbackContext context);
        void OnIzquierda(InputAction.CallbackContext context);
        void OnDerecha(InputAction.CallbackContext context);
        void OnDisparo(InputAction.CallbackContext context);
    }
}
